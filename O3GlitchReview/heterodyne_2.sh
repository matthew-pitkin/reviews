#!/bin/bash

# heterodyne the fake glitch data for LLO created with makefakeglitch_2.sh

# create frame cache file
frfiles=`ls -d $PWD/frames/L-L1*`

det=L1
psr=J0123+4500
parfile=J0123+4500.par
channel=${det}:FAKE_GLITCH

cachefile=${det}cache.lcf
segfile=${det}segments.txt

if [ -f $cachefile ]; then
  rm -f $cachefile
fi

if [ -f $segfile ]; then
  rm -f $segfile
fi

# create frame cache file and segment list file
touch $cachefile
touch $segfile
for frfile in $frfiles; do
  noext=${frfile%.*}
  gpstime=$(echo $noext | cut -d "-" -f 3)
  dur=$(echo $noext | cut -d "-" -f 4)
  echo "${det:0:1} $det $gpstime $dur file://localhost${frfile}" >> $cachefile
  echo "$gpstime $(($gpstime+$dur))" >> $segfile
done

# get the sample rate
read -r -a filearray <<< $frfiles
read -r -a samprate <<< `FrChannels ${filearray[0]}`
samplerate=${samprate[1]}

# run coarse heterodyne
coarseoutput=${PWD}/coarse_heterodyne_${psr}_${det}.txt.gz
exec=lalapps_heterodyne_pulsar
$exec -i $det -p $psr -z 0 -f $parfile -k 0.25 -s $samplerate -r 1 -d $cachefile -c $channel -o $coarseoutput -l $segfile -D 1024 -v

# run fine heterodyne
fineoutput=${PWD}/fine_heterodyne_${psr}_${det}.txt.gz
ebase=/home/matthew/repositories/lalsuite/lalpulsar/lib
ephemearth=${ebase}/earth00-40-DE405.dat.gz
ephemsun=${ebase}/sun00-40-DE405.dat.gz
$exec -i $det -p $psr -z 1 -f $parfile -k 0.25 -s 1 -r 1/60 -d $coarseoutput -o $fineoutput -l $segfile -e $ephemearth -S $ephemsun -v

# run fine heterodyne using par file that does not contain the glitch
parfileng=${psr}_noglitch.par
fineoutputng=${PWD}/fine_heterodyne_${psr}_${det}_noglitch.txt.gz
$exec -i $det -p $psr -z 1 -f $parfileng -k 0.25 -s 1 -r 1/60 -d $coarseoutput -o $fineoutputng -l $segfile -e $ephemearth -S $ephemsun -v

# run fine heterodyne using par file that does not contain the glitches fdot offset
parfilengfd=${psr}_noglitchfdot.par
fineoutputngfd=${PWD}/fine_heterodyne_${psr}_${det}_noglitchfdot.txt.gz
$exec -i $det -p $psr -z 1 -f $parfilengfd -k 0.25 -s 1 -r 1/60 -d $coarseoutput -o $fineoutputngfd -l $segfile -e $ephemearth -S $ephemsun -v