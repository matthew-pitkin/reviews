#!/bin/sh

exec=lalapps_Makefakedata_v5

# frame directory
frdir=frames
det=H1
channel=${det}:FAKE_GLITCH
band=128
name=fakeglitch

if [ ! -d $frdir ]; then
   mkdir $frdir
fi

sqrtSn=1e-24
dur=86400
refMJD=55000
refGPS=`python -c "import lalpulsar; print('{0:.9f}'.format(lalpulsar.TTMJDtoGPS(${refMJD})))"`
starttimefloat=`echo $refGPS-$dur | bc`
starttime=${starttimefloat%.*}

# frequency for first half of signal, with no glitch
spinfreq=29.8963954
hnaught=3.0e-24
phinaught=1.0  # initial GW phase
gwfreq=`echo 2*${spinfreq} | bc`
reftime=$(($starttime+$dur))
inj="{Alpha=0; Delta=0; Freq=$gwfreq; refTime=$refGPS; h0=$hnaught; cosi=0.1; psi=0.5; phi0=$phinaught;}"

$exec -F $frdir --outFrChannels=$channel -I $det --sqrtSX=$sqrtSn -G $starttime --duration=$dur --Band=$band --fmin=0 --injectionSources="${inj}" --outLabel=$name

# glitch frequency offset (EM values)
df=0.0001
newfreq=`echo ${gwfreq}+${df}*2 | bc`

# glitch phase offset (GW value, i.e. EM recovered value will be half of this)
dphi=2.0
newphase=`echo ${phinaught}+${dphi} | bc`

# create second half of signal after a "glitch" has occured (10 mins after ref/glitch time) adding glitch phase and frequency offset
newinj="{Alpha=0; Delta=0; Freq=$newfreq; refTime=$refGPS; h0=$hnaught; cosi=0.1; psi=0.5; phi0=$newphase;}"
starttimefloat=`echo $refGPS+600 | bc`
starttime=${starttimefloat%.*}
$exec -F $frdir --outFrChannels=$channel -I $det --sqrtSX=$sqrtSn -G $starttime --duration=$dur --Band=$band --fmin=0 --injectionSources="${newinj}" --outLabel=$name

# write out par file
parfile=J0000+0000.par
echo "PSRJ J0000+0000" > $parfile
echo "RAJ 00:00:00.0" >> $parfile
echo "DECJ 00:00:00.0" >> $parfile
echo "PEPOCH $refMJD" >> $parfile
echo "F0 $spinfreq" >> $parfile
echo "GLEP_1 $refMJD" >> $parfile
echo "GLF0_1 $df" >> $parfile
echo "GLPH_1 0" >> $parfile
echo "EPHEM DE405" >> $parfile
echo "UNITS TDB" >> $parfile

# write out a version without the glitch included
parfileng=J0000+0000_noglitch.par
echo "PSRJ J0000+0000" > $parfileng
echo "RAJ 00:00:00.0" >> $parfileng
echo "DECJ 00:00:00.0" >> $parfileng
echo "PEPOCH $refMJD" >> $parfileng
echo "F0 $spinfreq" >> $parfileng
echo "EPHEM DE405" >> $parfileng
echo "UNITS TDB" >> $parfileng