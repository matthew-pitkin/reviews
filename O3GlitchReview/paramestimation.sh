#!/bin/sh

# perform parameter estimation in various cases

det=H1
psr=J0000+0000
parfile=J0000+0000.par

# fine heterodyned data file
datafile=${PWD}/fine_heterodyne_${psr}_${det}.txt.gz

ebase=/home/matthew/repositories/lalsuite/lalpulsar/lib
ephemearth=${ebase}/earth00-40-DE405.dat.gz
ephemsun=${ebase}/sun00-40-DE405.dat.gz
ephemtime=${ebase}/tdb_2000-2040.dat.gz

# create prior file
priorfile=priors.txt
echo "H0 uniform 0 5e-23" > $priorfile
echo "PHI0 uniform 0 3.141592653589793" >> $priorfile
echo "COSIOTA uniform -1.0 1.0" >> $priorfile
echo "PSI uniform 0.0 1.5707963267948966" >> $priorfile
echo "GLPH_1 uniform 0 1" >> $priorfile

# set parameters
Nlive=1000
Ntraining=2000
outfile=nest_old.hdf5
postfile=post_old.hdf5

if [ -f $outfile ]; then
  rm -f $outfile
fi

# run parameter estimation with the old (un-fixed) code
execold=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/lalapps_pulsar_parameter_estimation_nested

$execold --detectors $det --par-file $parfile --input-files $datafile --outfile $outfile --prior-file $priorfile \
         --ephem-earth $ephemearth --ephem-sun $ephemsun --ephem-timecorr $ephemtime --Nlive $Nlive \
         --Nmcmcinitial 0 --roq --ntraining $Ntraining --verbose

ntwopexec=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/lalinference_nest2pos

if [ -f $postfile ]; then
  rm -f $postfile
fi

$ntwopexec -p $postfile $outfile

# run parameter estimation with the fixed code
outfile=nest_new.hdf5
postfile=post_new.hdf5
execnew=/home/matthew/miniconda2/envs/lalsuite-fix_glitch/bin/lalapps_pulsar_parameter_estimation_nested

if [ -f $outfile ]; then
  rm -f $outfile
fi

$execnew --detectors $det --par-file $parfile --input-files $datafile --outfile $outfile --prior-file $priorfile \
         --ephem-earth $ephemearth --ephem-sun $ephemsun --ephem-timecorr $ephemtime --Nlive $Nlive \
         --Nmcmcinitial 0 --roq --ntraining $Ntraining --verbose

if [ -f $postfile ]; then
  rm -f $postfile
fi

$ntwopexec -p $postfile $outfile