#!/bin/sh

# Make (another) fake glitch dataset (cf makefakeglitch.sh) - this time use the LLO
# detector, and add an fdot offset 

exec=lalapps_Makefakedata_v5

# frame directory
frdir=frames
det=L1
channel=${det}:FAKE_GLITCH
band=128
name=fakeglitch

if [ ! -d $frdir ]; then
   mkdir $frdir
fi

sqrtSn=3e-24
dur=86400
refMJD=55500
refGPS=`python -c "import lalpulsar; print('{0:.9f}'.format(lalpulsar.TTMJDtoGPS(${refMJD})))"`
starttimefloat=`echo $refGPS-$dur | bc`
starttime=${starttimefloat%.*}

# frequency for first half of signal, with no glitch
spinfreq=22.96532495
hnaught=2.8e-24
phinaught=0.5  # initial GW phase
gwfreq=`echo 2*${spinfreq} | bc`

# sky position (1h23m right ascension, 45d declination)
alpha=0.36215581978882333
delta=0.7853981633974483

inj="{Alpha=$alpha; Delta=$delta; Freq=$gwfreq; refTime=$refGPS; h0=$hnaught; cosi=-0.2; psi=0.8; phi0=$phinaught;}"

$exec -F $frdir --outFrChannels=$channel -I $det --sqrtSX=$sqrtSn -G $starttime --duration=$dur --Band=$band --fmin=0 --injectionSources="${inj}" --outLabel=$name

# glitch frequency offset (EM values)
df=0.00012
newfreq=`echo ${gwfreq}+${df}*2 | bc`

# glitch frequency derivative offset
dfdot=-0.000000013
newfdot=`echo ${dfdot}*2 | bc -l | awk '{printf "%.9f\n", $0}'`

# glitch phase offset (GW value, i.e. EM recovered value will be half of this)
dphi=1.5
newphase=`echo ${phinaught}+${dphi} | bc`

# create second half of signal after a "glitch" has occured (10 mins after ref/glitch time) adding glitch phase and frequency offset
newinj="{Alpha=$alpha; Delta=$delta; Freq=$newfreq; f1dot=$newfdot; refTime=$refGPS; h0=$hnaught; cosi=-0.2; psi=0.8; phi0=$newphase;}"
starttimefloat=`echo $refGPS+600 | bc`
starttime=${starttimefloat%.*}
$exec -F $frdir --outFrChannels=$channel -I $det --sqrtSX=$sqrtSn -G $starttime --duration=$dur --Band=$band --fmin=0 --injectionSources="${newinj}" --outLabel=$name

# write out par file
parfile=J0123+4500.par
echo "PSRJ J0123+4500" > $parfile
echo "RAJ 01:23:00.0" >> $parfile
echo "DECJ 45:00:00.0" >> $parfile
echo "PEPOCH $refMJD" >> $parfile
echo "F0 $spinfreq" >> $parfile
echo "GLEP_1 $refMJD" >> $parfile
echo "GLF0_1 $df" >> $parfile
echo "GLF1_1 $dfdot" >> $parfile
echo "GLPH_1 0" >> $parfile
echo "EPHEM DE405" >> $parfile
echo "UNITS TDB" >> $parfile

# write out a version without the glitch included
parfileng=J0123+4500_noglitch.par
echo "PSRJ J0123+4500" > $parfileng
echo "RAJ 01:23:00.0" >> $parfileng
echo "DECJ 45:00:00.0" >> $parfileng
echo "PEPOCH $refMJD" >> $parfileng
echo "F0 $spinfreq" >> $parfileng
echo "EPHEM DE405" >> $parfileng
echo "UNITS TDB" >> $parfileng

# write out version with glitch, but not including fdot offset
parfile=J0123+4500_noglitchfdot.par
echo "PSRJ J0123+4500" > $parfile
echo "RAJ 01:23:00.0" >> $parfile
echo "DECJ 45:00:00.0" >> $parfile
echo "PEPOCH $refMJD" >> $parfile
echo "F0 $spinfreq" >> $parfile
echo "GLEP_1 $refMJD" >> $parfile
echo "GLF0_1 $df" >> $parfile
echo "GLF1_1 0" >> $parfile
echo "GLPH_1 0" >> $parfile
echo "EPHEM DE405" >> $parfile
echo "UNITS TDB" >> $parfile