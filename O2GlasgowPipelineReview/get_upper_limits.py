#!/usr/bin/env python

"""
Get the upper limits on the amplitude parameter from a given posterior file.
"""

from __future__ import print_function

import os
import sys
import argparse
from lalapps.pulsarpputils import pulsar_nest_to_posterior, upper_limit_greedy

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--upper-limit', dest="ul", type=float,
                        default=0.95, help="Set the required upper limit credible interval (default: %(default)s)")
    parser.add_argument('-o', '--outfile', dest='outfile', required=True,
                        help="Set the file to output the upper limit(s) to")
    parser.add_argument('posterior', metavar='pos', nargs=1, help="The posterior file to be used")

    opts = parser.parse_args()

    if not os.path.isfile(opts.posterior[0]):
        print("Posterior file '{}' does not exist!".format(opts.posterior[0]), file=sys.stderr)
        sys.exit(1)

    try:
        pos, Zev, Nev = pulsar_nest_to_posterior(opts.posterior[0])
    except RuntimeError:
        print("Problem readingthe posterior file", file=sys.stderr)
        sys.exit(1)

    try:
        fp = open(opts.outfile, 'w')
    except IOError:
        print("Could not open output file '{}'!".format(opts.outfile), file=sys.stderr)
        sys.exit(1)

    if 'c22' in pos.names and 'c21' in pos.names:
        ulc22 = upper_limit_greedy(pos['c22'].samples, upperlimit=opts.ul)
        ulc21 = upper_limit_greedy(pos['c21'].samples, upperlimit=opts.ul)

        fp.write('C21: {}\n'.format(ulc21))
        fp.write('C22: {}\n'.format(ulc22))
    elif 'h0' in pos.names:
        ulh0 = upper_limit_greedy(pos['h0'].samples, upperlimit=opts.ul)

        fp.write('H0: {}\n'.format(ulh0))
    else:
        print("Posterior file does not contain required amplitude parameters", file=sys.stderr)
        sys.exit(1)

    fp.close()

