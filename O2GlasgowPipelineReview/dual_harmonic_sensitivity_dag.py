#!/usr/bin/env python

# Create DAG to run lalapps_pulsar_parameter_estimation_nested in both single
# harmonic (at twice the rotation frequency) and for two harmonics (once and
# twice the rotation frequency) on simulated noise to work out the
# "sensitivity" (expected 95% upper limits) on the C21 and C22 amplitudes
# for sources randomly distributed on the sky.

import os
import numpy as np
import uuid
from astropy import units as u
from astropy.coordinates import SkyCoord

# number of simulations
Nsims = 500

# create random sky positions (in rads)
ras = 2.*np.pi*np.random.rand(Nsims)
decs = -(np.pi/2.) + np.arccos(2.*np.random.rand(Nsims)-1.)

# set the pulsar search rotation frequency (the value of this does not matter)
f0 = 100.  # 100 Hz

# set the PSDs at the two frequencies
fpsd = 1e-48
twofpsd = 1e-48

# create prior file for dual harmonic search
priordual = "/home/sismp2/projects/sensitivity/prior_dual.txt"
fp = open(priordual, "w")
priorstr = """PHI21   uniform 0.0 6.2831853071795862
COSIOTA uniform -1.0 1.0
PHI22   uniform 0.0 6.2831853071795862
PSI     uniform 0.0 1.5707963267948966
C22     uniform 0.0 5e-23
C21     uniform 0.0 5e-23"""

fp.write(priorstr)
fp.close()

# create prior file for the single harmonic search
priorsingle = "/home/sismp2/projects/sensitivity/prior_single.txt"
fp = open(priorsingle, "w")
priorstr = """PHI0    uniform 0.0 3.141592653589793
COSIOTA uniform -1.0 1.0
PSI     uniform 0.0 1.5707963267948966
H0      uniform 0.0 1e-22"""

fp.write(priorstr)
fp.close()

# create .sub file for lalapps_pulsar_parameter_estimation_nested
substring = """universe = vanilla
executable = /home/sismp2/miniconda2/envs/master/bin/lalapps_pulsar_parameter_estimation_nested
arguments = " $(macrodetectors) --prior-file $(macroprior) --harmonics $(macroharmonics) --par-file $(macroparfile) $(macroinput) --outfile $(macrooutfile) --Nlive 1024 --Nmcmcinitial 0 "
getenv = true
request_memory = 2048
accounting_group_user = matthew.pitkin
accounting_group = ligo.sim.o2.cw.targeted.bayesian
log = /home/sismp2/projects/sensitivity/log/sensitivity.log
error = /home/sismp2/projects/sensitivity/log/ppe-$(cluster).err
output = /home/sismp2/projects/sensitivity/log/ppe-$(cluster).out
notifications = Never
queue 1
"""

ppesubfile = '/home/sismp2/projects/sensitivity/ppe.sub'
fp = open(ppesubfile, 'w')
fp.write(substring)
fp.close()

# create .sub file for lalinference_nest2pos
substring = """universe = vanilla
executable = /home/sismp2/miniconda2/envs/master/bin/lalinference_nest2pos
arguments = " --pos $(macropos) $(macroin) "
getenv = true
request_memory = 1024
accounting_group_user = matthew.pitkin
accounting_group = ligo.sim.o2.cw.targeted.bayesian
log = /home/sismp2/projects/sensitivity/log/sensitivity.log
error = /home/sismp2/projects/sensitivity/log/n2p-$(cluster).err
output = /home/sismp2/projects/sensitivity/log/n2p-$(cluster).out
notifications = Never
queue 1
"""

n2psubfile = '/home/sismp2/projects/sensitivity/n2p.sub'
fp = open(n2psubfile, 'w')
fp.write(substring)
fp.close()

# create .sub file for getting upper limits
substring = """universe = vanilla
executable = /home/sismp2/repositories/reviews/O2GlasgowPipelineReview/get_upper_limits.py
arguments = " --outfile $(macroout) --upper-limit 0.95 $(macroin) "
getenv = true
request_memory = 512
accounting_group_user = matthew.pitkin
accounting_group = ligo.sim.o2.cw.targeted.bayesian
log = /home/sismp2/projects/sensitivity/log/sensitivity.log
error = /home/sismp2/projects/sensitivity/log/ul-$(cluster).err
output = /home/sismp2/projects/sensitivity/log/ul-$(cluster).out
notifications = Never
queue 1
"""

ulsubfile = '/home/sismp2/projects/sensitivity/ul.sub'
fp = open(ulsubfile, 'w')
fp.write(substring)
fp.close()

# create .sub file for removing files
substring = """universe = local
executable = /bin/rm
arguments = " -f $(macrormfiles) "
accounting_group_user = matthew.pitkin
accounting_group = ligo.sim.o2.cw.targeted.bayesian
log = /home/sismp2/projects/sensitivity/log/sensitivity.log
error = /home/sismp2/projects/sensitivity/log/rm-$(cluster).err
output = /home/sismp2/projects/sensitivity/log/rm-$(cluster).out
notifications = Never
queue 1
"""

rmsubfile = '/home/sismp2/projects/sensitivity/rm.sub'
fp = open(rmsubfile, 'w')
fp.write(substring)
fp.close()

starttime = 900000000
duration = 86400
dt = 60

parstr = """PSRJ J0000+0000
RAJ {raj}
DECJ {decj}
F0 {f0}
PEPOCH 54000
C21 0.0
C22 0.0
COSIOTA 0.0
PSI 0.0
PHI21 0.0
PHI22 0.0
"""

dagfile = '/home/sismp2/projects/sensitivity/sensitivity.dag'
fpdag = open(dagfile, 'w')

sensratios = [0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 7.5, 10.]

if not os.path.isdir('/home/sismp2/projects/sensitivity/par'):
    os.makedirs('/home/sismp2/projects/sensitivity/par')
if not os.path.isdir('/home/sismp2/projects/sensitivity/dual'):
    os.makedirs('/home/sismp2/projects/sensitivity/dual')
if not os.path.isdir('/home/sismp2/projects/sensitivity/single'):
    os.makedirs('/home/sismp2/projects/sensitivity/single')

for k, sr in enumerate(sensratios):
    # perform loop, generate simulated noise and create DAG
    for i in range(Nsims):
        if not os.path.isdir('/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}'.format(i, k)):
            os.makedirs('/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}'.format(i, k))
        if not os.path.isdir('/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}'.format(i, k)):
            os.makedirs('/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}'.format(i, k))

        # create DAG IDs
        dualid = str(uuid.uuid4().hex)
        singleid = str(uuid.uuid4().hex)
        dualn2pid = str(uuid.uuid4().hex)
        singlen2pid = str(uuid.uuid4().hex)
        dualulid = str(uuid.uuid4().hex)
        singleulid = str(uuid.uuid4().hex)
        rmid = str(uuid.uuid4().hex)

        c = SkyCoord(ra=ras[i]*u.rad, dec=decs[i]*u.rad)

        # output par file
        parfdict = {}
        parfdict['f0'] = f0
        parfdict['raj'] = c.ra.to_string(unit=u.hourangle, sep=':')
        parfdict['decj'] = c.dec.to_string(unit=u.deg, sep=':')

        parfile = '/home/sismp2/projects/sensitivity/par/{0:05d}_{1:02d}.par'.format(i, k)
        fp = open(parfile, 'w')
        fp.write(parstr.format(**parfdict))
        fp.close()

        # create dual harmonic variables
        dualvars = ['macrodetectors="--fake-data H1 --fake-psd {},{}"'.format(fpsd*sr, twofpsd),
                    'macroharmonics="1,2"',
                    'macroinput="--inject-file {0} --inject-output /home/sismp2/projects/sensitivity/dual/{1:05d}/{2:02d}/injection.txt"'.format(parfile, i, k),
                    'macroparfile="{}"'.format(parfile),
                    'macrooutfile="/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/nest.hdf"'.format(i, k),
                    'macroprior="{}"'.format(priordual)]

        dualn2pvars = ['macropos="/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/post.hdf"'.format(i, k),
                       'macroin="/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/nest.hdf"'.format(i, k)]

        dualulvars = ['macroout="/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/ul.txt"'.format(i, k),
                      'macroin="/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/post.hdf"'.format(i, k)]

        fpdag.write('JOB {} {}\n'.format(dualid, ppesubfile))
        fpdag.write('VARS {} {}\n'.format(dualid, ' '.join(dualvars)))
        fpdag.write('JOB {} {}\n'.format(dualn2pid, n2psubfile))
        fpdag.write('VARS {} {}\n'.format(dualn2pid, ' '.join(dualn2pvars)))
        fpdag.write('JOB {} {}\n'.format(dualulid, ulsubfile))
        fpdag.write('VARS {} {}\n'.format(dualulid, ' '.join(dualulvars)))

        # create single harmonic variables
        singlevars = ['macrodetectors="--detectors H1"',
                      'macroharmonics="2"',
                      'macroinput="--input-files /home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/injection.txt_H1_2.0"'.format(i, k),
                      'macroparfile="{}"'.format(parfile),
                      'macrooutfile="/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/nest.hdf"'.format(i, k),
                      'macroprior="{}"'.format(priorsingle)]

        singlen2pvars = ['macropos="/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/post.hdf"'.format(i, k),
                         'macroin="/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/nest.hdf"'.format(i, k)]

        singleulvars = ['macroout="/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/ul.txt"'.format(i, k),
                        'macroin="/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/post.hdf"'.format(i, k)]

        fpdag.write('JOB {} {}\n'.format(singleid, ppesubfile))
        fpdag.write('VARS {} {}\n'.format(singleid, ' '.join(singlevars)))
        fpdag.write('JOB {} {}\n'.format(singlen2pid, n2psubfile))
        fpdag.write('VARS {} {}\n'.format(singlen2pid, ' '.join(singlen2pvars)))
        fpdag.write('JOB {} {}\n'.format(singleulid, ulsubfile))
        fpdag.write('VARS {} {}\n'.format(singleulid, ' '.join(singleulvars)))

        # remove nested sample files, posterior files and injection files
        rmfiles = ['/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/nest.hdf'.format(i, k),
                   '/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/nest_SNR'.format(i, k),
                   '/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/nest_Znoise'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/nest.hdf'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/nest_SNR'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/nest_Znoise'.format(i, k),
                   '/home/sismp2/projects/sensitivity/single/{0:05d}/{1:02d}/post.hdf'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/post.hdf'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/injection.txt_H1_1.0'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/injection.txt_H1_2.0'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/injection.txt_H1_1.0_signal_only'.format(i, k),
                   '/home/sismp2/projects/sensitivity/dual/{0:05d}/{1:02d}/injection.txt_H1_2.0_signal_only'.format(i, k)]

        fpdag.write('JOB {} {}\n'.format(rmid, rmsubfile))
        fpdag.write('VARS {} macrormfiles="{}"\n'.format(rmid, ' '.join(rmfiles)))

        # write child/parent
        fpdag.write('PARENT {} CHILD {}\n'.format(dualid, singleid))
        fpdag.write('PARENT {} CHILD {}\n'.format(dualid, dualn2pid))
        fpdag.write('PARENT {} CHILD {}\n'.format(dualn2pid, dualulid))
        fpdag.write('PARENT {} CHILD {}\n'.format(singleid, singlen2pid))
        fpdag.write('PARENT {} CHILD {}\n'.format(singlen2pid, singleulid))
        fpdag.write('PARENT {} {} CHILD {}\n'.format(dualulid, singleulid, rmid))

fpdag.close()

